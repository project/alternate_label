Description
The Alternate Label module allows you to add an alternate label to your cck 
fields while viewing the node. This module displays the default label while 
creating a content and a different label while displaying them. The purpose 
is to make forms more interactive. This module depends on CCK module.

Use case
You have a dropdown field with label "You are :" and options are "Scientist, 
Doctor, Engineer" but while you display in the node you should not display 
it as "You are: Student" instead display it as "Customer is: Engineer" or 
"Qualification: Engineer".

Installation
Install like any other module. You get to see a new fieldset on the nextpage 
after creating a new field. Just tick "Apply an alternate label" and then 
fill the text box with the alternate label you need to see.
